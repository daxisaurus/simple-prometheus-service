FROM python:3

COPY main.py /main.py
COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

CMD ["python", "/main.py"]
